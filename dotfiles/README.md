We observe that the proficient user often prefers tools which afford a
measure of customization, and that the most "hacker-like" among those
accrue some collection of time-honed configuration files.

This section may, at your option, contain either:

  a) A collection of dotfiles or other configuration from your working environment.

  b) A rationale for your lack of same.

There is no right/wrong response (or depth of response).

