# A9, LLC Preliminary Application

#### Join a Fledgling Managed Services Organization

## About this repository

This repo is our application. It lets us gauge your ability and familiarity with the requirements of the role.


## How to use this repository

1. Fork this repository.
2. Follow the instructions in the `README` files contained within each top level directory.
3. Send a pull request back to A9, LLC.

Note that pull requests can either be sent back to the main repository or you can create a branch on your fork and create a pull request into master on your fork.

### What Happens To Your Pull Request

Your pull request will *never* be merged (to keep this repository open for the applicants that use it after you). When you've either been hired or declined your pull request will simply be closed.

During the review process you may find comments applied to your pull request - asking questions, seeking clarification, a rogue compliment, etc.

### Legal Stuff Regarding Your Pull Request

**No content from your pull request will be used by A9, LLC for any purpose other than assessing the capability of a given applicant.**

We won't rip off your code. You retain copyright on any pull request you submit.

However, by submitting a pull request, you affirm that the content within is solely your original work. Content generated collaboratively with other individuals must be identified as such and content generated entirely by other individuals should not be included. Failure to meet these guidelines may result in disqualification as an applicant.

## Job Description

### About the Company

[A9, LLC](https://www.a9group.net) A9 Develops and Supports Custom Linux and Atlassian tools deployments. This group will provide services to
be used internally by the companny for development, as well as support the production platforms in use by our clients.

### About the Department

In addition to the sales and support of the Atlassian tools:

- JIRA
- Confluence
- Bitbucket
- Bamboo
- Fisheye/Crucible
- Crowd
- Bamboo


The department also design, build and implement release and automation pipelines which incorporate tools and products like:

- Jenkins
- Hudson
- FogBugz
- Bugzilla
- Subversion
- Perforce
- Team Concert


We're looking for individuals with skills in these areas:

 - Customer Service Specialist
	You want to have a hand in building (and eventually *running*) a
	successful customer service organization in cloud services; live
	support, SLA response protocols, relationship building

 - Systems Architecture
	You want to have a hand in building (and eventually *running*) a
	successful systems organization from the ground up; virtual
	machines, the software on them, the storage and data connected
	to them, all the way through to the edge firewall (and their
	wholistic relationships)

 - Atlassian Applications
	- Certified JIRA and Confluence administrators preferred.
	- 2-5 years experience supporting and extending JIRA and Confluence installations

### The Daily Workflow
Day-to-day duties will include attending status meetings, completing
tasks in Atlassian JIRA, learning, listening, *doing*.

JIRA is key, and the Engineer who wants to be involved in SDLC will
benefit from an in-depth understanding of Atlassian JIRA and how it
connects with other products to enable and empower our customers
(especially the ones who aren't our customers *yet*)

You'll need experience with Linux, TCP/IP networking, storage. It would
be awesome if you can demonstrate experience with these in conjunction
with automation and/or virtualization.

Every day will contain at least one meeting. This may be a short as 5
minutes to give a brief (extremely brief) "go/no-go" or binary response,
or as long as it takes to get the task accomplished.

Being a participant on a marathon Skype or Conference Bridge session
*will happen*. How often remains to be seen.

You will read and write documentation to support the Team's efforts.
This will be in Atlassian Confluence at a minimum, and perhaps in your
code. (1) (Sarah Maddox is held in bard-like regard.)

You may be asked to single-handedly respond to an issue, providing
updates and relevant information whenever possible. This issue(s) will
constitute your 'deliverable' for our meetings. You *own* these, and
it's your responsibility (and delight) to see them to completion or
through continued growth.

It's a terrific opportunity for a person who wants first-hand experience
creating a user experience; workflows, user interface elements,
service response, et cetera. 

### Required Qualifications

* Familiarity with Linux-based systems
* Experience working with modern version control systems such as Git
* Experience with modern software development tools and processes

### Preferred Qualifications

* Experience with Atlassian Tools (Confluence, JIRA, Crowd, Bamboo, Stash..)
* Certification from Atlassian on JIRA or Confluence
* Experience administering virtual systems like OpenStack, VirtualBox, Parallels, etc.
* Experience with administering networking and routing for dozens of VLANs / hundreds of users
* Experience with server configuration management systems like Puppet, Chef, Salt, Ansible


## Legalese

This repository is licensed under the [MIT License](http://opensource.org/licenses/mit-license.php):

Copyright (c) 2016 A9, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THIS SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
